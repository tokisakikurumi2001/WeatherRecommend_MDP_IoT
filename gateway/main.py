import sys
import time
import serial.tools.list_ports

from Adafruit_IO import *

AIO_FEED_ID = "+"	# Adafruit IO feed Id
AIO_USERNAME = AIOUSERNAME	# Adafruit IO username
AIO_KEY = AIOKEY	# Adafruit IO api key


def connected(client):
    print("Successfully connected...")
    client.subscribe(AIO_FEED_ID)


def subscribe(client , userdata , mid , granted_qos):
    print("Successfully subscribed...")


def disconnected(client):
    print("Disconnected...")
    sys.exit (1)


def message(client , feed_id , payload):
    print("Sent: " + payload)


client = MQTTClient(AIO_USERNAME , AIO_KEY)
client.on_connect = connected
client.on_disconnect = disconnected
client.on_message = message
client.on_subscribe = subscribe
client.connect()
client.loop_background()

# Accessing to micro:bit's serial device
def getPort():
    ports = serial.tools.list_ports.comports()
    N = len (ports)
    commPort = "None"
    for i in range(0, N):
        port = ports [i]
        strPort = str(port)
        if "USB Serial Device" in strPort :
            splitPort = strPort.split (" ")
            commPort = (splitPort [0])
    return commPort

# Have to run after micro:bit to search for USB Serial Device
ser = serial.Serial(port = getPort(), baudrate = 115200)


def processData(data):
    data = data.replace("!", "")
    data = data.replace("#", "")

    splitData = data.split(":")
    print(splitData)
    if splitData[1] == "TEMP":
        client.publish("mdp-temp", splitData[2])
    elif splitData[1] == "HUMID":
        client.publish("mdp-humid", splitData[2])
    elif splitData[1] == "WIND":
        client.publish("mdp-wind", splitData[2])


mess = ""
def readSerial():
    bytesToRead = ser.inWaiting()
    if(bytesToRead > 0):
        global mess
        mess = mess + ser.read(bytesToRead).decode("UTF-8")
        while("#" in mess) and ("!" in mess):
            start = mess.find ("!")
            end = mess.find ("#")
            processData(mess[start:end + 1])
            if(end == len(mess)):
                mess = ""
            else:
                mess = mess[end + 1:]


while True:
    time.sleep(6)
    readSerial()
